package pt.simov.trackyourkids.services;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Moreira on 28/12/2017.
 */

public interface ICurrentLocation {

    /**
     * sets current location
     * @param pLocation
     */
    void setCurrentLocation(LatLng pLocation);
}
