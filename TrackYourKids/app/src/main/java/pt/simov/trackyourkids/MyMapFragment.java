package pt.simov.trackyourkids;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import pt.simov.trackyourkids.rest.ChildreenService;
import pt.simov.trackyourkids.models.Child;
import pt.simov.trackyourkids.models.Zone;
import pt.simov.trackyourkids.services.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Moreira on 29/10/2017.
 */

public class MyMapFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap map;
    private Child child;
    private final int minRadius = 100;
    private final int maxRadius = 3200;
    private final int increment = 2;
    private Circle circle = null;
    private Marker marker = null;

    private final String TAG = this.getClass().getName().toUpperCase();

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {

        super.onCreateView(layoutInflater, viewGroup, bundle);

        View view = layoutInflater.inflate(R.layout.map_fragment, viewGroup, false);
        SupportMapFragment supportFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportFragment.getMapAsync(this);
        Button btSave = (Button) view.findViewById(R.id.btSave);
        Button btChildLocation = (Button) view.findViewById(R.id.btGoToLocation);

        TextView txtChildName = (TextView) view.findViewById(R.id.txtChildName);
        txtChildName.setText(child.getName().toUpperCase());

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Please wait...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();
                ChildreenService service = new ChildreenService();

                final Zone zone = new Zone(circle.getCenter().latitude, circle.getCenter().longitude, (int) circle.getRadius());
                Log.d(TAG, zone.toString());
                UserService userService = new UserService();
                String username = userService.getCurrentUser(getContext());
                Call<String> call = service.AddZone(username, child.getName(), zone);

                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        progressDialog.cancel();
                        Log.d(TAG, "" + response.isSuccessful());
                        Log.d(TAG, "" + response.code());
                        Log.d(TAG, "" + response.message());
                        Log.d(TAG, "" + response.raw().toString());
                        Log.d(TAG, "" + response.toString());
                        child.getAllowedZone().setLatitute(zone.getLatitute());
                        child.getAllowedZone().setLongitude(zone.getLongitude());
                        child.getAllowedZone().setRadius(zone.getRadius());
                        new AlertDialog.Builder(getContext())
                                .setTitle("Feedback")
                                .setMessage("Area was saved")
                                .setNeutralButton("OK", null)
                                .create()
                                .show();
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable throwable) {
                        progressDialog.cancel();

                        new AlertDialog.Builder(getContext())
                                .setTitle("Feedback")
                                .setMessage("Wasn't possible to save the area.")
                                .setNeutralButton("OK", null)
                                .create()
                                .show();
                    }
                });
            }
        });

        btChildLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Please wait...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();

                UserService userService = new UserService();
                ChildreenService childreenService = new ChildreenService();
                childreenService.requestChildLastLocation(userService.getCurrentUser(getContext()), child.getName())
                        .enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                                progressDialog.cancel();

                                if (response.isSuccessful()) {
                                    new AlertDialog.Builder(getContext())
                                            .setTitle("Feedback")
                                            .setMessage("The location for " + child.getName() + " was requested.")
                                            .setNeutralButton("OK", null)
                                            .create()
                                            .show();
                                } else {
                                    onFailure(null, null);
                                }


                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable throwable) {
                                progressDialog.cancel();

                                new AlertDialog.Builder(getContext())
                                        .setTitle("Feedback")
                                        .setMessage("Wasn't possible to request the location for " + child.getName())
                                        .setNeutralButton("OK", null)
                                        .create()
                                        .show();
                            }
                        });
            }
        });

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setTrafficEnabled(true);
        map.setIndoorEnabled(true);
        map.setBuildingsEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setOnMarkerClickListener(this);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(39.585660, -8.409959), 7));

        if (this.child != null && this.child.getAllowedZone().getRadius() != 0) {
            CircleOptions circleOptions = createCircle(this.child.getAllowedZone().getCoordinates(), this.child.getAllowedZone().getRadius());
            circle = map.addCircle(circleOptions);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(this.child.getAllowedZone().getCoordinates());
            markerOptions.draggable(true);
            marker = map.addMarker(markerOptions);
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(this.child.getAllowedZone().getCoordinates(), getZoom(this.child.getAllowedZone().getRadius())));

            Log.d(TAG, this.child.getAllowedZone().toString());
        }

        map.setOnCircleClickListener(new GoogleMap.OnCircleClickListener() {
            @Override
            public void onCircleClick(Circle circle) {
                if (circle.getRadius() < maxRadius) {
                    circle.setRadius(circle.getRadius() * increment);
                } else {
                    circle.setRadius(minRadius);
                }

                map.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), getZoom((int) circle.getRadius())));

            }
        });


        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                circle.setCenter(marker.getPosition());

            }

            @Override
            public void onMarkerDrag(Marker marker) {

                circle.setCenter(marker.getPosition());
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

            }
        });

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                if (marker != null) {
                    removeMarker();
                }

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.draggable(true);

                CircleOptions circleOptions = createCircle(latLng, minRadius);

                marker = map.addMarker(markerOptions);
                circle = map.addCircle(circleOptions);

                map.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), getZoom((int) circle.getRadius())));


            }
        });
    }


    @Override
    public boolean onMarkerClick(Marker marker) {

        removeMarker();
        return true;
    }

    public void setChild(Child pChild) {
        this.child = pChild;
    }

    private void removeMarker() {
        marker.remove();
        circle.remove();
    }

    private int getZoom(int radius) {
        int zoom = 20;

        switch (radius) {
            case maxRadius:
                zoom = 12;
                break;
            case 400:
            case 800:
                zoom = 14;
                break;
            case 1600:
                zoom = 13;
                break;
            default:
                zoom = 16;
                break;

        }

        return zoom;
    }

    private CircleOptions createCircle(LatLng pLatLng, int pRadius) {

        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(pLatLng);
        circleOptions.radius(pRadius);
        circleOptions.clickable(true);
        circleOptions.fillColor(Color.rgb(255, 204, 204));
        circleOptions.strokeColor(Color.TRANSPARENT);
        return circleOptions;
    }

}


