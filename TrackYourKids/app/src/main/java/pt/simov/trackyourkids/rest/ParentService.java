package pt.simov.trackyourkids.rest;

import pt.simov.trackyourkids.converter.NullOnEmptyConverterFactory;
import pt.simov.trackyourkids.models.Parent;
import pt.simov.trackyourkids.models.Token;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Moreira on 04/11/2017.
 */

public class ParentService {


    private IParentService client;

    public  ParentService(){
        build();
    }

    private void build() {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://simov-tracking.herokuapp.com/")
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        client = retrofit.create(IParentService.class);
    }

    public Call<String> register(String pUsername, String pPassword) {
        Parent parent= new Parent(pUsername,pPassword);
        Call<String> call = client.register(parent);
        return call;
    }

    public  Call<Void> updateToken(String pUsername, Token pToken){
        Call<Void> call= client.refreshToken(pUsername,pToken);
        return  call;
    }


}
