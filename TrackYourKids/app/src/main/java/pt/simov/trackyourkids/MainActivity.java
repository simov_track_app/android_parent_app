package pt.simov.trackyourkids;

import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import pt.simov.trackyourkids.models.Child;
import pt.simov.trackyourkids.rest.ChildreenService;
import pt.simov.trackyourkids.services.UserService;

public class MainActivity extends AppCompatActivity implements ChildreenListFragment.OnChildSelectedListener,
        RegistrationFragment.OnRegistrationSuccessListener {

    private final String TAG = this.getClass().getName().toUpperCase();
    private boolean mWasPermissionAlreadyAsked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (mWasPermissionAlreadyAsked) {
                finish();
            }
            mWasPermissionAlreadyAsked = true;
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    0);
            return;
        }
        UserService userService = new UserService();
        String username = userService.getCurrentUser(this);
        int option = username == "" ? 1 : 2;
        setFragment(option);

    }


    private void setFragment(int pMenuOption) {
        Fragment fragment = null;

        switch (pMenuOption) {

            case 1:
                fragment = new RegistrationFragment();
                break;

            case 2:
                fragment = new ChildreenListFragment();
                break;


        }


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btLogout:
                UserService userService = new UserService();
                userService.logout(this);
                //clean stack
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                setFragment(1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onChildSelected(Child pChild) {
        MyMapFragment mapFragment = new MyMapFragment();
        ChildreenService childService= new ChildreenService();
        UserService userService= new UserService();
        childService.requestChildLastLocation(userService.getCurrentUser(this),pChild.getName());
        mapFragment.setChild(pChild);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, mapFragment)
                .addToBackStack(null)
                .commit();
    }


    @Override
    public void OnRegistrationSuccess(String pUsername) {

        UserService userService = new UserService();
        boolean result = userService.saveUser(this, pUsername);

        if (result) {
            setFragment(2);
        }

    }
}
