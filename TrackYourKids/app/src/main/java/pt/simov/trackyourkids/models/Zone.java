package pt.simov.trackyourkids.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Moreira on 18/11/2017.
 */

public class Zone {

    private double locationLatitude;
    private double locationLongitude;
    private int radiusMeters;

    public Zone(double latitute, double longitude, int radius) {
        this.locationLatitude = latitute;
        this.locationLongitude = longitude;
        this.radiusMeters = radius;
    }

    public double getLatitute() {
        return locationLatitude;
    }

    public void setLatitute(double latitute) {
        this.locationLatitude = latitute;
    }

    public double getLongitude() {
        return locationLongitude;
    }

    public void setLongitude(double longitude) {
        this.locationLongitude = longitude;
    }

    public int getRadius() {
        return radiusMeters;
    }

    public void setRadius(int radius) {
        this.radiusMeters = radius;
    }

    public LatLng getCoordinates(){
        return new LatLng(this.getLatitute(),this.getLongitude());
    }

    @Override
    public String toString() {
        return "Zone{" +
                "locationLatitude=" + locationLatitude +
                ", locationLongitude=" + locationLongitude +
                ", radiusMeters=" + radiusMeters +
                '}';
    }
}
