package pt.simov.trackyourkids;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import pt.simov.trackyourkids.rest.ChildreenService;
import pt.simov.trackyourkids.models.Child;
import pt.simov.trackyourkids.services.UserService;
import pt.simov.trackyourkids.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Moreira on 28/10/2017.
 */

public class ChildreenListFragment extends Fragment {

    private ListView listView;
    private ListAdatper listAdatper;

    private OnChildSelectedListener listener;
    private ProgressDialog progressDoalog;
    private final String TAG = this.getClass().getName().toUpperCase();

    public interface OnChildSelectedListener {
        public void onChildSelected(Child pChild);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.childreen_list, container, false);
        listView = (ListView) view.findViewById(R.id.childreenList);

        Log.d(TAG, "OnCreateView");

        boolean isNetOn = Utils.isNetOn(getContext());

        if (isNetOn) {
            load();
        } else {
            new AlertDialog.Builder(getContext())
                    .setTitle("Error")
                    .setMessage("Connection to internet is required!")
                    .setNeutralButton("OK", null)
                    .create()
                    .show();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Child child = (Child) listAdatper.getItem(i);
                if (child != null) {
                    listener.onChildSelected(child);
                }

            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void load() {
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMessage("Its loading...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setIndeterminate(true);
        progressDoalog.setCancelable(false);
        progressDoalog.show();

        ChildreenService service = new ChildreenService();
        UserService userService = new UserService();
        String username = userService.getCurrentUser(getContext());
        Call<ArrayList<Child>> call = service.GetChildreen(username);

        call.enqueue(new Callback<ArrayList<Child>>() {
            @Override
            public void onResponse(Call<ArrayList<Child>> call, Response<ArrayList<Child>> response) {
                Log.d(TAG, "OK");
                ArrayList<Child> childreen = response.body();
                Log.d(TAG, childreen != null ? "" + childreen.size() : "0");

                if (childreen == null) {
                    childreen = new ArrayList<>(0);
                }
                listAdatper = new ListAdatper(childreen);
                listView.setAdapter(listAdatper);
                progressDoalog.hide();
            }

            @Override
            public void onFailure(Call<ArrayList<Child>> call, Throwable throwable) {
                Log.d(TAG, "not OK");
                Log.d(TAG, throwable.getMessage());

                listAdatper = new ListAdatper(new ArrayList<Child>(0));
                listView.setAdapter(listAdatper);

                new AlertDialog.Builder(getContext())
                        .setTitle("Error")
                        .setMessage("Could not get your list. Try again later.")
                        .setNeutralButton("OK", null)
                        .create()
                        .show();

                progressDoalog.hide();

            }
        });


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnChildSelectedListener) context;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }

    }
}
