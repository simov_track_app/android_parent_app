package pt.simov.trackyourkids.rest;

import java.util.ArrayList;

import pt.simov.trackyourkids.models.Child;
import pt.simov.trackyourkids.models.Zone;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Moreira on 18/11/2017.
 */

public interface IChildreenService {

    @GET("/parent/{name}/childrenTracked")
    Call<ArrayList<Child>> GetChildreen(@Path("name") String name);

    @GET("/parent/{name}/childreen/{childName}")
    Call<Child> GetChild(@Path("name") String name, @Path("childName") String childName);

    @POST("/parent/{name}/children/{childName}/zone")
    Call<String> AddZone(@Path("name") String name, @Path("childName")String childName, @Body Zone zone);

    @Headers({"Content-Type: application/json"})
    @POST("/parent/{name}/children/{childName}/last-location")
    Call<Void> requestChildLastLocation(@Path("name") String parentName,@Path("childName") String childName);
}
