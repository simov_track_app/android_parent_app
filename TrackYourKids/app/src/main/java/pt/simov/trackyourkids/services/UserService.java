package pt.simov.trackyourkids.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Moreira on 22/12/2017.
 */

public class UserService {


    /**
     * Returns the current user
     * @param pContext
     * @return
     */
    public String getCurrentUser(Context pContext) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(pContext);
        String username = sharedPref.getString("Username", "");
        return username;
    }

    /**
     * Deletes all information about the current user
     * @param pContext
     * @return
     */
    public  boolean logout(Context pContext){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(pContext);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove("Username");
        return editor.commit();
    }

    /**
     * Saves the current user
     * @param pContext
     * @param pUsername
     * @return
     */
    public boolean saveUser(Context pContext, String pUsername){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(pContext);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Username", pUsername);
        return  editor.commit();
    }

    /**
     * Saves the FCM token for the current user
     * @param pContext
     * @param pRefreshedToken
     * @return
     */
    public boolean saveFCMToken(Context pContext, String pRefreshedToken){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(pContext);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("fcmToken", pRefreshedToken);
        return editor.commit();
    }

    /**
     * Saves the current location for the user
     * @param pContext
     * @param pCurrentLocation
     * @return
     */
    public boolean saveCurrentLocation(Context pContext, LatLng pCurrentLocation){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(pContext);
        SharedPreferences.Editor editor = sharedPref.edit();
        String location=""+pCurrentLocation.latitude+";"+pCurrentLocation.longitude;
        editor.putString("currentLocation", location);
        return editor.commit();
    }

    /**
     * Gets the last knoe location for the current user
     * @param pContext
     * @return
     */
    public LatLng getlastKnowLocation(Context pContext){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(pContext);
        String location = sharedPref.getString("currentLocation", "");
        Log.d("UserService", "Location: " + location);
        String[] tmp=location.split(";");
        LatLng currentLocation=new LatLng(Double.parseDouble(tmp[0]),Double.parseDouble(tmp[1]));
        return currentLocation;
    }

    public String getFCMToken(Context pContext) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(pContext);
        String username = sharedPref.getString("fcmToken", "");
        return username;
    }
}
