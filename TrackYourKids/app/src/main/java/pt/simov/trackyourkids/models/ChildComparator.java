package pt.simov.trackyourkids.models;

import java.util.Comparator;

/**
 * Created by Moreira on 01/01/2018.
 */

public class ChildComparator implements Comparator<Child> {
    @Override
    public int compare(Child child, Child t1) {
        return child.getName().compareTo(t1.getName());
    }
}
