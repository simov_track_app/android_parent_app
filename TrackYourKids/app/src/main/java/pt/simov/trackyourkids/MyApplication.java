package pt.simov.trackyourkids;

import android.app.Application;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;

import pt.simov.trackyourkids.models.Token;
import pt.simov.trackyourkids.rest.ParentService;
import pt.simov.trackyourkids.services.ICurrentLocation;
import pt.simov.trackyourkids.services.LocationService;
import pt.simov.trackyourkids.services.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Moreira on 28/12/2017.
 */

public class MyApplication extends Application {

    private final String TAG = this.getClass().getName().toUpperCase();
    private LocationService locationService;

    @Override
    public void onCreate() {
        super.onCreate();

        final UserService userService = new UserService();

        String username = userService.getCurrentUser(getBaseContext());
        final String fcmToken = userService.getFCMToken(getBaseContext());

        Log.d(TAG, "Username: " + username);
        Log.d(TAG, "Token: " + fcmToken);

        ParentService parentService = new ParentService();

        // Update token in backend
       if (userService.getCurrentUser(getBaseContext()) != null) {
            parentService.updateToken(username, new Token(fcmToken)).enqueue(
                    new Callback<Void>() {
                        @Override
                        public void onResponse(Call<Void> call, Response<Void> response) {
                            if (response.isSuccessful()) {
                                Log.d(TAG, "Successfully updated FCM token: " + fcmToken);
                            } else {
                                Log.d(TAG, "Failed to update FCM token");
                                try {
                                    Log.d(TAG, response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Void> call, Throwable t) {
                            t.printStackTrace();
                            Log.d(TAG, "Failed to update FCM token");
                        }
                    }
            );
        }

        locationService = new LocationService(getBaseContext(), new ICurrentLocation() {
            @Override
            public void setCurrentLocation(LatLng pLocation) {
                userService.saveCurrentLocation(getBaseContext(), pLocation);
            }
        });
    }
}
