package pt.simov.trackyourkids;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import pt.simov.trackyourkids.rest.ParentService;
import pt.simov.trackyourkids.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Moreira on 04/11/2017.
 */

public class RegistrationFragment extends Fragment {


    private final String TAG = this.getClass().getName().toUpperCase();
    private ProgressDialog progressDoalog;
    private int mode;

    public interface OnRegistrationSuccessListener {
        void OnRegistrationSuccess(String pUsername);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
    }

    private OnRegistrationSuccessListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.registration, container, false);

        final EditText txtUsername = (EditText) view.findViewById(R.id.txtUsername);
        txtUsername.setFocusable(true);
        final EditText txtPassword = (EditText) view.findViewById(R.id.txtPassword);
        final Button btRegister = (Button) view.findViewById(R.id.btRegister);
        final Context context = getContext();
        final TextView txtLinkSign = (TextView) view.findViewById(R.id.txtLinkSign);

        txtLinkSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mode == 0) {
                    btRegister.setText(getString(R.string.sign_in));
                    txtLinkSign.setText(getString(R.string.login));
                    mode = 1;
                } else {
                    btRegister.setText(getString(R.string.login));
                    txtLinkSign.setText(getString(R.string.create_account));
                    mode = 0;
                }


            }
        });

        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isNetOn = Utils.isNetOn(context);
                if (isNetOn) {
                    register(txtUsername.getText().toString(), txtPassword.getText().toString());
                } else {
                    new AlertDialog.Builder(getContext())
                            .setTitle("Error")
                            .setMessage("Connection to internet is needed!")
                            .setNeutralButton("OK", null)
                            .create()
                            .show();
                }


            }
        });


        return view;
    }

    private void register(String pUsername, String pPassword) {
        final String username = pUsername;
        String password = pPassword;
        boolean isValid = isValid(username, password);

        if (isValid) {

            ParentService service = new ParentService();
            Log.d(TAG.toUpperCase(), username + "-" + password);

            progressDoalog = new ProgressDialog(getContext());
            progressDoalog.setMessage("Please wait...");
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDoalog.setIndeterminate(true);
            progressDoalog.setCancelable(false);
            progressDoalog.show();

            Call<String> call = service.register(username, password);

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    Log.d(TAG, "" + response.isSuccessful());
                    Log.d(TAG, "" + response.code());
                    progressDoalog.cancel();
                    if (mode == 1 && response.isSuccessful()) {
                        listener.OnRegistrationSuccess(username);
                    } else if (mode == 0 && (response.code() == 405 || response.code() == 409)) {
                        listener.OnRegistrationSuccess(username);
                    } else {
                        new AlertDialog.Builder(getContext())
                                .setTitle("Error")
                                .setMessage("Could not create user. Try a different username or try again later.")
                                .setNeutralButton("OK", null)
                                .create()
                                .show();

                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable throwable) {
                    progressDoalog.cancel();
                    new AlertDialog.Builder(getContext())
                            .setTitle("Error")
                            .setMessage("Could not create user. Try a different username or try again later.")
                            .setNeutralButton("OK", null)
                            .create()
                            .show();

                }
            });


        }
    }

    private boolean isValid(String pUsername, String pPassword) {
        boolean isValid = true;

        if (pUsername.isEmpty()) {
            Toast.makeText(getContext(), "Username is mandatory", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (pUsername.length() < 5) {
            Toast.makeText(getContext(), "Username needs to have at least 5 characters", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (pPassword.isEmpty()) {
            Toast.makeText(getContext(), "Password is mandatory", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (pPassword.length() < 5) {
            Toast.makeText(getContext(), "Password needs to have at least 5 characters", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        return isValid;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (OnRegistrationSuccessListener) context;

        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }
}
