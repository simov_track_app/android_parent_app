package pt.simov.trackyourkids.rest;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import pt.simov.trackyourkids.converter.NullOnEmptyConverterFactory;
import pt.simov.trackyourkids.models.Child;
import pt.simov.trackyourkids.models.Zone;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Moreira on 28/10/2017.
 */

public class ChildreenService {

    private IChildreenService client;

    public ChildreenService() {
        Build();
    }

    private void Build() {

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://simov-tracking.herokuapp.com/")
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        client = retrofit.create(IChildreenService.class);
    }


    public Call<ArrayList<Child>> GetChildreen(String pParentId) {
        Call<ArrayList<Child>> childreen = client.GetChildreen(pParentId);
        return childreen;
    }


    public Call<Child> GetChild(String pParentId,String pChildId) {
        Call<Child> child = client.GetChild(pParentId, pChildId);
        return child;
    }

    public Call<String> AddZone(String pParentId, String pChildId,Zone pZone) {
        Call<String> child = client.AddZone(pParentId,pChildId, pZone);
        return child;
    }

    public  Call<Void> requestChildLastLocation(String pUsername, String pChildUsername){
        Call<Void> call= client.requestChildLastLocation(pUsername,pChildUsername);
        return  call;
    }


}
