package pt.simov.trackyourkids.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import pt.simov.trackyourkids.services.INotificationService;
import pt.simov.trackyourkids.services.NormalNotificationService;
import pt.simov.trackyourkids.services.NotificationFactory;

/**
 * Created by Moreira on 02/12/2017.
 */

public class FirebaseListenerService extends FirebaseMessagingService {

    private final String TAG = this.getClass().getName().toUpperCase();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "Message received!");

        INotificationService notificationService = NotificationFactory.getNotificationService(remoteMessage);

        notificationService.sendNotification(
                this.getApplicationContext(),
                remoteMessage);
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }
}