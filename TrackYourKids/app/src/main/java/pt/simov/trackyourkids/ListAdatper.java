package pt.simov.trackyourkids;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import pt.simov.trackyourkids.models.Child;
import pt.simov.trackyourkids.models.ChildComparator;

/**
 * Created by Moreira on 28/10/2017.
 */

public class ListAdatper extends BaseAdapter {

    private ArrayList<Child> childreen;


    public ListAdatper(ArrayList<Child> pChildreen) {
        Collections.sort(pChildreen, new ChildComparator());
        childreen = pChildreen;
    }

    @Override
    public int getCount() {
        return childreen.size();
    }

    @Override
    public Object getItem(int i) {
        return childreen.get(i);
    }

    @Override
    public long getItemId(int i) {
        return -1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Child child = childreen.get(i);
        View itemView = null;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) viewGroup.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(android.R.layout.simple_list_item_1, null);
        } else {
            itemView = view;
        }

        // Set the text of the row
        TextView txtChildName = (TextView) itemView.findViewById(android.R.id.text1);
        txtChildName.setText("" + child.getName());


        return itemView;
    }
}
