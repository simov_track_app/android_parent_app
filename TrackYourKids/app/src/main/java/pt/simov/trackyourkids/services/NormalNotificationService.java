package pt.simov.trackyourkids.services;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;

import pt.simov.trackyourkids.R;

/**
 * Created by Moreira on 27/12/2017.
 */

public class NormalNotificationService implements INotificationService {

    @Override
    public void sendNotification(Context context, RemoteMessage remoteMessage) {

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, notificationBuilder.build());
    }
}
