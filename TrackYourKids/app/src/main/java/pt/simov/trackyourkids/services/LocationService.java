package pt.simov.trackyourkids.services;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

/**
 * Created by Moreira on 27/12/2017.
 */

public class LocationService {

    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Retrives the current location
     * @param pContext
     * @param pCurrentLocation
     */
    @SuppressLint("MissingPermission")
    public LocationService(Context pContext, final ICurrentLocation pCurrentLocation) {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(pContext);

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            pCurrentLocation.setCurrentLocation(new LatLng(location.getLatitude(), location.getLongitude()));
                        }
                    }
                });
    }

}
