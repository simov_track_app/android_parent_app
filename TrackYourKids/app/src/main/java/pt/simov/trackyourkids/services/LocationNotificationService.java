package pt.simov.trackyourkids.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.RemoteMessage;

import pt.simov.trackyourkids.MyApplication;
import pt.simov.trackyourkids.R;

/**
 * Created by Moreira on 27/12/2017.
 */

public class LocationNotificationService implements INotificationService {

    private final String TAG = this.getClass().getName().toUpperCase();

    @Override
    public void sendNotification(Context context, RemoteMessage message) {
        LatLng childLocation = new LatLng(
                Double.valueOf(message.getData().get("lat")),
                Double.valueOf(message.getData().get("long")));

        AddressService addressService = new AddressService();
        String address = addressService.getAddress(context, childLocation);
        UserService userService = new UserService();
        LatLng currentLocation = userService.getlastKnowLocation(context);
        Log.d(TAG, address);
        String geoUriString = "http://maps.google.com/maps?saddr=" +
                currentLocation.latitude + "," +
                currentLocation.longitude + "&daddr=" +
                childLocation.latitude + "," +
                childLocation.longitude;

        Intent notificationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUriString));

        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        String childName = message.getData().get("childName").toUpperCase();
        String message2 = childName + " current location is: " + address;

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message2);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(childName + " Current Location")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(message2)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message2))
                .setContentIntent(intent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = notificationBuilder.build();

        notificationManager.notify(1, notification);

    }
}
