package pt.simov.trackyourkids.utils;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import pt.simov.trackyourkids.ChildreenListFragment;
import pt.simov.trackyourkids.MainActivity;
import pt.simov.trackyourkids.R;

/**
 * Created by Moreira on 02/12/2017.
 */

public class Utils {


    public static boolean isNetOn(Context pContext) {
        ConnectivityManager connMgr = (ConnectivityManager) pContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isNetOn = wifi.isConnectedOrConnecting() || mobile.isConnectedOrConnecting();
        return isNetOn;
    }

}
