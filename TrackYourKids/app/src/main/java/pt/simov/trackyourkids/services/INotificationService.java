package pt.simov.trackyourkids.services;

import android.content.Context;

import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Moreira on 27/12/2017.
 */
public interface INotificationService {
    /**
     * Sends a notification
     *
     * @param context
     * @param remoteMessage
     */
    void sendNotification(Context context, RemoteMessage remoteMessage);
}
