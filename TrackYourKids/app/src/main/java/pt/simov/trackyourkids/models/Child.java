package pt.simov.trackyourkids.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Moreira on 28/10/2017.
 */

public class Child  {

    private String id;
    private String parentUsername;
    private String childUsername;
    private float lastLocationLatitude;
    private float lastLocationLongitude;
    private Zone allowedZone;


    public String getId() {
        return id;
    }

    private void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return childUsername;
    }

    private void setName(String name) {
        this.childUsername = name;
    }

    public String getParentId() {
        return parentUsername;
    }

    private void setParentId(String parentId) {
        this.parentUsername = parentId;
    }


    public float getLastLocationLatitude() {
        return lastLocationLatitude;
    }

    public void setLastLocationLatitude(float lastLocationLatitude) {
        this.lastLocationLatitude = lastLocationLatitude;
    }

    public float getLastLocationLongitude() {
        return lastLocationLongitude;
    }

    public void setLastLocationLongitude(float lastLocationLongitude) {
        this.lastLocationLongitude = lastLocationLongitude;
    }

    public LatLng getLastLocation(){
        return  new LatLng(this.getLastLocationLatitude(),this.getLastLocationLongitude());
    }

    public Zone getAllowedZone() {
        return allowedZone;
    }

    public void setAllowedZone(Zone allowedZone) {
        this.allowedZone = allowedZone;
    }

}
