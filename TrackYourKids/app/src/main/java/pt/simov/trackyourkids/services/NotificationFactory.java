package pt.simov.trackyourkids.services;

import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Moreira on 27/12/2017.
 */

public class NotificationFactory {

    public static INotificationService getNotificationService(RemoteMessage remoteMessage) {
        INotificationService notificationService = null;

        if (remoteMessage.getData() != null && remoteMessage.getData().containsKey("lat")) {
            notificationService = new LocationNotificationService();
        } else {
            notificationService = new NormalNotificationService();
        }

        return notificationService;
    }

}
