package pt.simov.trackyourkids.models;

/**
 * Created by Moreira on 02/12/2017.
 */

public class Token {

    private String fcmToken;

    public Token(String fcmToken) {
        this.setFcmToken(fcmToken);
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
