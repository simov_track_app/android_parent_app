package pt.simov.trackyourkids.services;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Moreira on 28/12/2017.
 */

public class AddressService {


    /**
     * Return the address from a specifc coordinates
     * @param pContext
     * @param pCoordinates
     * @return
     */
    public String getAddress(Context pContext, LatLng pCoordinates) {

        Geocoder geocoder = new Geocoder(pContext, Locale.getDefault());
        String street = null;
        try {
            Address address = geocoder.getFromLocation(pCoordinates.latitude, pCoordinates.longitude, 1).get(0);
            street = address.getAddressLine(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return street;
    }
}
