package pt.simov.trackyourkids.rest;


/**
 * Created by Moreira on 18/11/2017.
 */

import pt.simov.trackyourkids.models.Parent;
import pt.simov.trackyourkids.models.Token;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IParentService {

    @POST("/parent")
    Call<String> register(@Body Parent parent);

    @PUT("/parent/{name}/token")
    Call<Void> refreshToken(@Path("name") String parentName, @Body Token pToken);

}
