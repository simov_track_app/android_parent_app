package pt.simov.trackyourkids.firebase;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telecom.Call;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import pt.simov.trackyourkids.MainActivity;
import pt.simov.trackyourkids.R;
import pt.simov.trackyourkids.models.Token;
import pt.simov.trackyourkids.rest.ParentService;
import pt.simov.trackyourkids.services.UserService;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Moreira on 02/12/2017.
 */

public class InstanceIDListenerService extends FirebaseInstanceIdService {

    private final String TAG = this.getClass().getName().toUpperCase();

    @Override
    public void onTokenRefresh() {
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        final UserService userService = new UserService();
        String username = userService.getCurrentUser(getApplicationContext());
        userService.saveFCMToken(getBaseContext(), refreshedToken);

        if (!username.equals("")) {
            ParentService service = new ParentService();
            Token token = new Token(refreshedToken);
            service.updateToken(username, token).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(retrofit2.Call<Void> call, Response<Void> response) {
                    userService.saveFCMToken(getBaseContext(),refreshedToken);
                    Log.d(TAG, "onTokenRefresh OK");
                }

                @Override
                public void onFailure(retrofit2.Call<Void> call, Throwable throwable) {
                    Log.d(TAG, "onTokenRefresh NOT OK");
                }
            });
        }
    }
}