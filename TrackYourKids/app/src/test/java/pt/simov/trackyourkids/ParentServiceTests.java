package pt.simov.trackyourkids;

import junit.framework.Assert;

import org.junit.Test;

import pt.simov.trackyourkids.rest.ParentService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Moreira on 30/12/2017.
 */

public class ParentServiceTests {

    private final String PARENT = "parent_user";

    @Test
    public void loginTest(){
        ParentService parentService= new ParentService();
       Call<String> call=parentService.register("parent_user","qwerty");
       call.enqueue(new Callback<String>() {
           @Override
           public void onResponse(Call<String> call, Response<String> response) {
               boolean isOk= response.isSuccessful()|| response.code()==405|| response.code()==409;
               Assert.assertEquals(true, isOk);
           }

           @Override
           public void onFailure(Call<String> call, Throwable throwable) {
               Assert.fail();
           }
       });
    }
}
