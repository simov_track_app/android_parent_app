package pt.simov.trackyourkids;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

import pt.simov.trackyourkids.models.Child;
import pt.simov.trackyourkids.models.Zone;
import pt.simov.trackyourkids.rest.ChildreenService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Moreira on 27/12/2017.
 */

public class ChildServiceTests {

    private final String PARENT = "parent_user";
    private final String CHILD = "child_user";

    @Test
    public void getChildreenTest() {
        ChildreenService service = new ChildreenService();
        Call<ArrayList<Child>> call = service.GetChildreen(PARENT);
        call.enqueue(new Callback<ArrayList<Child>>() {
            @Override
            public void onResponse(Call<ArrayList<Child>> call, Response<ArrayList<Child>> response) {

                Assert.assertEquals(true, response.isSuccessful());
                Assert.assertEquals(3, response.body().size());
            }

            @Override
            public void onFailure(Call<ArrayList<Child>> call, Throwable throwable) {
                Assert.fail();
            }
        });
    }

    @Test
    public void getChildTest() {
        ChildreenService service = new ChildreenService();
        Call<Child> call = service.GetChild(PARENT, CHILD);

        call.enqueue(new Callback<Child>() {
            @Override
            public void onResponse(Call<Child> call, Response<Child> response) {
                Assert.assertEquals(true, response.isSuccessful());
                Assert.assertEquals(CHILD, response.body().getName());

            }

            @Override
            public void onFailure(Call<Child> call, Throwable throwable) {
                Assert.fail();
            }
        });
    }

    @Test
    public void addZoneTest() {
        ChildreenService service = new ChildreenService();
        Zone zone = new Zone(10.100, 10.100, 999);
        Call<String> call = service.AddZone(PARENT, CHILD, zone);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Assert.assertEquals(true, response.isSuccessful());
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {

            }
        });
    }

    @Test
    public void requestChildLocationTest() {
        ChildreenService service = new ChildreenService();
        Call<Void> call = service.requestChildLastLocation(PARENT, CHILD);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Assert.assertEquals(true, response.isSuccessful());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {
                Assert.fail();
            }
        });
    }

}
